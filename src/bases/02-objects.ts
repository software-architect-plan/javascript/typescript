
export const pokemonIds = [1, 20, 30, 34, 66]



interface Pokemon {
    id: number;
    name: string;
    age: number;
    // age?: number; // ? es nullable
}

export const bulbasaur: Pokemon = {
    id: 1,
    name: 'Bulbasaur',
    age: 1
}

export const charmander: Pokemon = {
    id: 4,
    name: "charmander",
    age: 5
}

console.log(bulbasaur)

// + es un parse
// pokemonIds.push( +'1' )
// console.log(pokemonIds)

export const pokemons: Pokemon[] = [];

pokemons.push(charmander, bulbasaur)

console.log(pokemons)
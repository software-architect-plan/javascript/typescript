export let name: string = 'Felipe'
export const age: number = 30
export const isValid: boolean = true

name = 'Fabiola'
// name = 123
// name = true

export const templateString = ` Esto es un string
multilinea
que puede tener
" dobles 
' simple
inyectar valores ${ name }
expresiones ${ 1 + 1}
numeros: ${ age }
booleanos: ${ isValid }
`

console.log(templateString.length)